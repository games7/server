"use strict";
exports.__esModule = true;
var WSEvents;
(function (WSEvents) {
    WSEvents["connection"] = "connection";
    WSEvents["disconnect"] = "disconnect";
    WSEvents["message"] = "message";
    WSEvents["tictactoe"] = "tictactoe";
    WSEvents["startGame"] = "startGame";
    WSEvents["tictactoeRedirection"] = "tictactoeRedirection";
    WSEvents["move"] = "move";
})(WSEvents = exports.WSEvents || (exports.WSEvents = {}));
