export interface Player {
    id: string;
    playerName?: string;
    registered: boolean;
}