"use strict";
exports.__esModule = true;
var express = require("express");
var socketio = require("socket.io");
var path = require("path");
var Games_1 = require("./Games");
var WebSocketEvents_1 = require("./WebSocketEvents");
var clientFolder = "./dist/client";
var indexPage = "index.html";
var app = express();
//app.set("port", process.env.PORT || 3000);
var http = require("http").Server(app);
// set up socket.io and bind it to our http server.
var io = socketio(http);
var players = [];
// workaround, to keep track of waiting players
var waitingPlayers = 0;
app.get("/", function (req, res) {
    res.sendFile(path.resolve(clientFolder + "/" + indexPage));
});
app.use(express.static(clientFolder));
app.use(function (req, res) {
    res.sendFile(path.join(__dirname, clientFolder + "/" + indexPage));
});
// whenever a user connects on port 3000 via
// a websocket, log that a user has connected
io.on(WebSocketEvents_1.WSEvents.connection, function (socket) {
    players.push({ id: socket.id, registered: false });
    // Greet and invite to pick a nickname
    socket.on(WebSocketEvents_1.WSEvents.message, function (message) {
        console.log(message);
        socket.emit("message", "You just send: " + message);
    });
    socket.on(WebSocketEvents_1.WSEvents.disconnect, function () {
        console.log(socket.id + " got disconnected");
        //remove from players array
    });
    socket.on(WebSocketEvents_1.WSEvents.tictactoe, function (message) {
        console.log("Tic tac toe message received: " + message.game);
        socket.emit(WebSocketEvents_1.WSEvents.message, "tictactoe: " + message.playerName);
    });
    // TODO: Game routing
    socket.on(WebSocketEvents_1.WSEvents.startGame, function (message) {
        switch (message.game) {
            case Games_1.Games.TicTacToe:
                console.log(message.playerName + " joins " + message.game + " queue");
                HandleTicTacToeGameRequest(socket);
                break;
            case Games_1.Games.Chess:
            case Games_1.Games.Otello:
            default:
                socket.emit(WebSocketEvents_1.WSEvents.message, "This game has not been developed yet");
                break;
        }
    });
    socket.on(WebSocketEvents_1.WSEvents.move, function (message) {
        console.log("Tic tac toe message received: " + message.game);
        socket.emit(WebSocketEvents_1.WSEvents.message, "tictactoe: " + message.playerName);
    });
});
var server = http.listen(3000, function () {
    console.log("listening on *:3000");
});
function HandleTicTacToeGameRequest(socket) {
    waitingPlayers++;
    if (waitingPlayers < 2) {
        socket.emit(WebSocketEvents_1.WSEvents.message, "You joined the queue to play Tic Tac Toe!");
        console.log("waitingPlayers", waitingPlayers);
    }
    else if (waitingPlayers === 2) {
        socket.emit(WebSocketEvents_1.WSEvents.tictactoeRedirection);
        // TODO: player id harcoded!!!!
        socket.broadcast.to(players[0].id).emit(WebSocketEvents_1.WSEvents.tictactoeRedirection);
        socket.emit(WebSocketEvents_1.WSEvents.message, "Start the game!");
        socket.broadcast.to(players[0].id).emit(WebSocketEvents_1.WSEvents.message, "Start the game!");
    }
    else if (waitingPlayers > 2) {
        socket.emit(WebSocketEvents_1.WSEvents.message, "There is no more room for more players :(");
    }
}
